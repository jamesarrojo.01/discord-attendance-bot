/**
 * A Lambda function that replies to interaction with static string
 */

 const { globalHandler } = require('../handler.js')


 exports.data = {
   name: 'logout',
   type: 1,
   description: 'replies with your logout time.'
 }
 
 const action = async (body) => {
   // May do something here with body
   // Body contains Discord command details
   try {
     let today = new Date()
      function calcTime() {
          let utc = today.getTime() + (today.getTimezoneOffset() * 60000)
  
          let nd = new Date(utc + (3600000 * 8))
  
          return nd.toLocaleString('en-US', {
            // en-US can be set to 'default' to use user's browser settings
            hour: '2-digit',
            minute: '2-digit',
          })
      }
      let loggedInTime = calcTime()

      // add code here to save login time to database


  
     let response = {
       "content": `👏 Thank you for your hard work, **${body.member.nick}**! You logged out at ${loggedInTime}`
     }
     return response
   } catch (error) {
    return error
   }
   
 }
 
 exports.handler = (event) => {
   globalHandler(event, action)
 }
 